# Comix Spider

A web crawler that scans predefined web sites, downloads new images from them, 
stores images and texts locally and sends periodic updates to subscribers. The
main use case is to subscribe to get web comics by email.

## Installation
1. Clone the repo somewhere you can run a cron on it (a Linux server is
   preferable).
2. Setup the database connection strings in `.dbsecrets.json` - you can start
   by copying `.dbsecrets.json.example` to `.dbsecrets.json` and edit the file.
3. Run `./comix test-image-support` to setup and test dependencies (dependencies
   are installed into the `.bundle` sub-directory). This test will error out if
   it can't handle common image formats - see note about resize support below for
   details.
4. Run `./comix --help` and use the CLI to set up the web sites to scan and the
   patterns to look for (using regular expressions or XPath expressions). The
   CLI is mostly self documenting and shouldn't be hard to figure out.
5. Setup cron to run `./comix` (without parameters) to perform the scan. It is
   recommended to not run this more than twice an hour or some such, though the
   system backsoff automatically and doesn't hammer the sites more than needed
   or configured (each feed has its own delay policy).

The cron job should be set up to send emails to the administrator - the main script
(without the debug flag) will only output text when there are errors, and cron
will by default email the administrator only if the script outputs some text.

### Note about image resize support

The Comix Spider uses the Ruby vips gem to resize images if they are too large to be sent
over email. This is done opportunisticly - only of the SMTP server refuses to
accept an email message. The vips gem is installed through bundler and will automatically
download a copy of the VIPS library source code and build it. This has several caveates:
1. The first run will take some time, it will take a lot of CPU and memory - if
  you are running this on a constrained system - it may fail.
2. The VIPS library is configured automatically to use whatever image processing libraries
  are available on your system - if your system installation is missing development
  headers for important image processing libraries, VIPS will not build with them and
  will not support resizing images for the missing libraries, instead when Comix Spider
  tries to use VIPS to resize such images - it will crash. It is recommended to make
  sure you have at least `libjpeg-dev`, `libpng-dev` and `libwebp-dev` before trying
  to install the bundle.

I'm looking for alternatives that are less brittle - if you have any suggestions, send
me a note.

## License

Copyright: Oded Arbel <oded@geek.co.il>  
Published under the GNU GENERAL PUBLIC LICENSE version 3 or later (see COPYING
file for details).
