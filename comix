#!/usr/bin/env ruby

Dir.chdir File.dirname(__FILE__)
$: << "."

begin
	require 'bundler/setup'
rescue Exception => e
	$stderr.puts "Bundle is not set up yet, setting it up now - please wait a few minutes for the setup to complete..."
	$stderr.puts ""
	system('bundle config set --local path .bundle')
	system('bundle install')
	exec($0, *ARGV)
end

require 'logger'

$logger = Logger.new(STDERR)
$logger.level = Logger::WARN if ARGV.select { |a| next if $doneopts; $doneopts = (a[0] != '-'); a == '-d' }.empty?
$MAX_POLLS_PER_RUN = 10
$SMTP_SERVER = ENV['SMTP_SERVER'] || 'localhost'
$SMTP_PORT = (ENV['SMTP_PORT'] || 25).to_i
$SMTP_SENDER_NAME  = 'Daily Comix'
$SMTP_SENDER_EMAIL = 'oded-comix@geek.co.il'

require 'cmdparse'
require 'dbmodel'

# CLI commands go here
require 'user-commands'
require 'feed-commands'
require 'image-utils'

class RunCommand < CmdParse::Command
	attr :firstonly
	attr :demomode
	attr :nobackoff
	
	def initialize
		super 'run', takes_commands: false
		@firstonly = @demomode = @nobackoff = false
		self.short_desc = "Process all comics feeds and mail results to users"
		self.long_desc = "When called without any command, #{$0} will run this command to process all feeds and users."
		self.options do |opt|
			opt.on("-1", "Stop processing after the first feed that was polled (and wasn't in backoff)") {|f| @firstonly = true }
			opt.on("-nCOUNT", "--max=COUNT", "Maximum number of polled feeds (default: 10)") { |c| $MAX_POLLS_PER_RUN = c.to_i }
			opt.on("-f", "Demo mode to test stuff") {|f| @demomode = true }
			opt.on("-t", "Disable backoff (useful for testing)") {|f| @nobackoff = true }
		end
	end	

	def execute
		if (@demomode) 
		    u = ComixUser.getUser('oded arbel');
		    u.last_sent = Time.at(Time.now().to_i-43200);
		    u.getNewItems.each { |i| puts i }
		    return
		end
		# process a bunch of comics
		polled = 0
		ComixFeed.nextToPoll do |feed|
			next unless feed.poll(@nobackoff)
			break if @firstonly
			polled += 1
			break if polled >= $MAX_POLLS_PER_RUN
		end
		# mail to users
		ComixUser.all.each do |user| 
			user.mailNewItems
		end
	end
end

$max_payload_size = 0 ## maybe we don't need max size ? it was 4194304

cmd = CmdParse::CommandParser.new(handle_exceptions: :no_help)
cmd.main_options.program_name = "comix"
cmd.main_options.version = [1, 0, 0]
cmd.global_options do |opt|
  opt.separator "Global options:"
  opt.on("-nCOUNT", "--max=COUNT", "Maximum number of polled feeds (default: 10)") { |c| $MAX_POLLS_PER_RUN = c.to_i }
  opt.on("-v", "--verbose", "Be verbose when outputting info") {|t| $logger.level = Logger::INFO }
  opt.on("-d", "--debug", "Output debugging info") {|t| $logger.level = Logger::DEBUG }
  opt.on("-D", "--debug-sql", "Output SQL debugging info") {|t| ActiveRecord::Base.logger = Logger.new(STDERR) }
end
cmd.add_command( CmdParse::HelpCommand.new )
cmd.add_command( UsersCommand.new )
cmd.add_command( FeedsCommand.new )
cmd.add_command( InitDBCommand.new )
cmd.add_command( TestImagesCommand.new )
cmd.add_command( RunCommand.new, default: true)

cmd.parse
