for strip in \
    'No Need For Bushido' Nahast 'Treasure Hunters' 'Butternut Squash' 'Theater Hopper' 'Rob and Elliot' \
    'Joe and Monkey' 'Krakow' 'Marilith' 'Red String' 'Bigger Than Cheeses' 'Joe Loves Crappy Movies' \
    'Matriculated' 'Nothing Nice to Say' 'The Asylumantics' 'Crap I Drew on My Lunch Break' 'Sam and Fuzzy' \
    'Cartridge Comics' 'F@NBOY$' 'PC Weenies' 'Dueling Analogs' 'Exiern' 'Sins' 'Three Panel Soul' \
    'Pulse' 'JoeGP' 'Peacekeepers' 'Abandon: First Vampire' 'Blank It' 'Gunnerkrigg Court' \
    'Brat-halla' 'Weregeek' 'Undertow' 'Another Video Game Webcomic' 'Tales of Pylea' 'Not Alone' \
    'Simulated comic Product' 'No Crap!' 'Deverish Also' 'Precocious' 'The War of Winds' 'Lint' \
    'Serenian Century' 'Planet Karen' "Finder's Keepers" 'Back on Earth' 'JEFBOT' 'anti-HEROES' Catalyst \
    'Trying Human' 'The Meek' 'By Moon Alone - Bonus Pages' 'By Moon Alone' 'Fantasy Realms' 'No Rest For The Wicked' \
    'Strays' 'Cat Legend'
do
    ./comix feed enable "$strip"
    #echo $strip
done

