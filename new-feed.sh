#!/bin/bash
name="$1"; shift
url="$1"; shift

(
    cd $(dirname $0)
    ./comix feed add "$name" "$url"
    ./comix feed parsers add "$name" strip '(?i)<img[^>]+src=["'"'"']?([^"'"'"'> ]*'"$1"'[^"'"'"'> ]*)'
    ./comix user subscriptions add 'Oded Arbel' "$name"
    ./comix -d feed poll -f "$name"
)
