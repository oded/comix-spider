#!/bin/false

require 'net/http'
require 'net/smtp'
require 'time'
require 'digest/md5'
require 'rmail'
require 'cgi'
require 'etc'
require 'base64'
require 'resolv'
require 'addressable'

require 'image-utils'

# monkey-patch Net::HTTP to help me check for XML
module Net
	class HTTPResponse
		def xml?
			if content_type =~ %r{^(?:text|application)/(?:.+\+|)xml$}
				true
			else
				false
			end
		end
	end
end

def parseURI(url)
	return url if url.is_a? Addressable::URI
	url = url.to_s if url.is_a? URI
	Addressable::URI.parse(url)
end

def fetch(url, limit = 10, headers = nil)
	raise HTTPBadResponse, 'HTTP redirect too deep' if limit == 0
	raise StandardError, '#fetch only takes Addressable::URI input' unless url.is_a? Addressable::URI

	$logger.debug("Getting #{url}")
	addrs = Resolv::DNS.new.getaddresses(url.host).sort { |a,b| b.class.to_s <=> a.class.to_s }.collect { |ip| ip.to_s }
	begin
		raise "Failed to connect to #{url}" if addrs.empty?
		ipaddr = addrs.shift
		$logger.debug "Connecting to #{url.scheme}://#{ipaddr}:#{url.inferred_port}"
		response = Net::HTTP.start(url.host, url.inferred_port, :use_ssl => url.scheme == 'https', open_timeout: 3, ipaddr: ipaddr) do |http|
			req = Net::HTTP::Get.new(url.display_uri.request_uri)
			#req['User-Agent'] = 'Mozilla/4.0 (Compatible)'
			req['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'
			req['Cookie'] = 'age_gate=18'
			headers.each { |k,v| req[k.capitalize] = v } unless headers.nil?
			http.request(req)
		end
	rescue Timeout::Error => e
		retry # restart and get a new IP
	end

	case response
	when Net::HTTPSuccess     then
		response
	when Net::HTTPRedirection then
		location = canonize(response['location'], url)
		$logger.debug("Redirecting to: #{location}")
		fetch(location, limit - 1)
	else
		$logger.debug("Fetch failed unexpectadly! #{response.error!}")
		response.error!
	end
end

def canonize(url, baseurl)
	baseurl = Addressable::URI.parse(baseurl) unless baseurl.is_a? Addressable::URI
	# unxmlize the URL
	url.gsub!(/\s+/,'') unless url.index("\n").nil?
	url.gsub!(/\s+$/,'')
	url.gsub!('&amp;','&')
	url.gsub!('&#038;', '&')
	url.gsub!(/[“”]|%E2%80%9[CD]/i, '')
	# let addressable do all the heavy lifting
	begin
		return baseurl + Addressable::URI.parse(url)
	rescue Addressable::URI::InvalidURIError => e
		$logger.error "failed to canonize url #{url}: #{e}"
		return url
	end
end

def md5sum(text)
	return Digest::MD5.hexdigest(text)
end

class Net::HTTPResponse
	def date
		# this is too smart for me - usually I just want to know when I've seen the new content, not when
		# it claims itself to be posted
		#return Time.httpdate(fetch('last-modified')) if key?('last-modified')
		#return Time.httpdate(fetch('date')) if key?('date')
		return Time.now
	end
	
	def md5
		return Digest::MD5.hexdigest(body)
	end
end
