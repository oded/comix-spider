#!/bin/bash

comix="$1"
parser="$2"

[ -z "$comix" ] && exit 5
[ -z "$parser" ] && exit 5
cwd="$(dirname $0)"

while [ "$($cwd/comix feed parsers list "$comix" | wc -l)" -gt "0" ]; do
    $cwd/comix feed parsers del "$comix" 1
done

$cwd/comix feed parsers add "$comix" strip '(?i)<img[^>]+src=["'"'"']([^"'"'"'>]*'"$parser"'[^"'"'"'>]+)["'"'"']'
$cwd/comix -d feed poll -f "$comix"

