#!/bin/false

require 'vips'
require 'cmdparse'

require 'net-utils'

module Comix
	module ImageUtils
		class MissingImageImplementation < StandardError
		end
		
		def scaleImage buffer, filename, maxsize, logname
			begin
				oldimg = Vips::Image.new_from_buffer buffer, ''
				$logger.debug { "Resizing #{logname} - #{oldimg.size}" }
				scale = maxsize.to_f/oldimg.size.max
				newimg = oldimg.resize(scale)
				$logger.debug { "Result #{logname} - #{newimg.size}" }
				newimg.write_to_buffer(filename)
			rescue Vips::Error => e
				raise MissingImageImplementation, "Cannot load image for #{filename}" if e.message.include? 'is not a known buffer format'
				raise e
			end
		end
		
		module_function :scaleImage
	end
end

class TestImagesCommand < CmdParse::Command
	@@sample_images = {
		'jpeg' => 'https://filesamples.com/samples/image/jpeg/sample_640%C3%97426.jpeg',
		'png' => 'https://filesamples.com/samples/image/png/sample_640%C3%97426.png',
		'webp' => 'https://filesamples.com/samples/image/webp/sample1.webp',
		#'heic' => 'https://filesamples.com/samples/image/heic/sample1.heic',
		#'avif' => 'https://raw.githubusercontent.com/link-u/avif-sample-images/master/hato.profile0.8bpc.yuv420.avif',
	}
	
	def initialize
		super 'test-image-support', takes_commands: false
		self.short_desc = 'Test that minimal image support is available through the VIPS library'
		self.long_desc = 'Verify that the VIPS library used can load images of the most popular types
		used in web comics, by loading some samples from the internet.'
	end
	
	def execute()
		failed = false
		@@sample_images.each do |type,url|
			$logger.debug "Testing #{type} using #{url}"
			fname = File.basename(url)
			img = fetch(parseURI(url))
			throw StandardError, "Error getting sample image from #{url}" unless img.is_a? Net::HTTPOK
			begin
				res = Comix::ImageUtils.scaleImage img.body, fname, 200.0, "Test sample (#{type}) #{fname}"
				$logger.debug "Inage converted successfully from size #{img.body.size}b to #{res.size}b"
			rescue => e
				$logger.error "Failed to convert image of type #{type}"
				failed = true
			end
		end
		exit 1 if failed
	end
end
