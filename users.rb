#!/bin/false

require 'email-utils'

class ComixUser < ActiveRecord::Base
	has_and_belongs_to_many :registrations, :class_name => "ComixFeed", 
						:join_table => "comix_registrations", 
						:foreign_key => "userid", 
						:association_foreign_key => "comixid", 
						:uniq => true, :select => "comix_feeds.*"
	def to_s() "#{name} <#{email}> #{admin ? "[admin]" : ""}" end
	
		# load one feed record
	def ComixUser.getUser(name)
		users = ComixUser.where("name = ? OR email = ?", name, name)
		if users.length != 1
			raise CmdParse::InvalidArgumentError, users.empty? ? 
					"No user records matching '#{name}' found!" : 
					"More then one user record matching #{name} was found, aborting!"
		end
		return users.first
	end

	def getNewItems 
		startdate = last_sent
		if startdate.nil? then 
			startdate = Time.at(0) 
		end
		$logger.debug("Looking for new items for #{name} since #{startdate}")
		items = ComixStrip.joins(feed: [ :registrations ]).where("comix_registrations.userid = ? AND comix_strips.date > ?", self[:id], startdate).limit(20) +
				ComixRant.joins(feed: [ :registrations ]).where("comix_registrations.want_rants != 0 AND comix_registrations.userid = ? AND comix_rants.date > ?", self[:id], startdate)
		items.sort! { |a,b| a.feed <=> b.feed }
		return items
		
		registrations.each do |feed|
			next unless feed.active
			if feed.updated.nil? or feed.updated < startdate then
				#$logger.debug("Feed #{feed} wasn't updated")
				next
			end
			$logger.debug("Found new content for #{feed}")
			feed.getNewStrips(startdate).each { |s| items << s; $logger.debug("New strip #{s.filename}"); }
			feed.getNewRants(startdate).each { |r| items << r }
		end
		$logger.debug("Total #{items.length} new items found")
		return items
	end
	
	def mailNewItems(fake = false, force = false)
		return false unless active
		# make sure I don't mail too often
#		if !force and !last_sent.nil? and last_sent + schedule > Time.now then
#		    $logger.debug("Backoff #{email}")
#		    return false
#		end
		ComixUser.transaction do
			items = getNewItems
			if items.empty? and message.nil? then
				$logger.info("No new content for #{self}")
				return false
			end
			$logger.debug("Preparing new email to #{self}")
			# its ok to update last_sent this early, because the transaction would rollback if an error occurs
			update_attribute('last_sent', Time.now) 
			newmsg = Comix::ComixMessage.new(self, fake, $max_payload_size)
			lastfeed = nil
			items.each do |item|
				$logger.debug("Figuring out item #{item}")
				if (email_size > 0 and newmsg.count >= email_size) and lastfeed != item.feed
					$logger.info("Sending message sized #{newmsg.payloadSize}")
					newmsg.send
					newmsg = Comix::ComixMessage.new(self,fake, $max_payload_size)
				end
				
				begin
					if ! newmsg.addItem(item)
						$logger.error("Item is too big: #{item}")
						newmsg.addItem(ComixText.new(item.feed, "Strip is too big to send, go to the website to see it."))
					end
				rescue FeedError => e
					$logger.error("Error sending item #{item}: #{e}")
				end
				lastfeed = item.feed
			end
			unless message.nil?
				newmsg.addItem(ComixText.new(nil, message))
				update_attribute('message',nil)
			end
			$logger.info("Sending last message sized #{newmsg.payloadSize}")
			newmsg.send unless newmsg.nil?
		end		
	end
	
	def destroy(id)
	    raise FeedError,"Not allowed to destroy users !"
	end

	def delete(id)
	    raise FeedError,"Not allowed to delete users !"
	end
end

class ComixText
	def initialize(feed, message)
	    @feed = feed
	    @message = message
	end
	
	def data
	    @message
	end
	
	def feed
	    @feed
	end
end
