#!/bin/false

require 'image-utils'

class String
	def to_quoted_printable(*args)
		[self].pack("M").gsub(/\n/, "\r\n")
	end
	def from_quoted_printable
		self.gsub(/\r\n/, "\n").unpack("M").first
	end
end

module Comix
	class ComixMessageFeed
		attr_reader :name, :url, :rants, :text, :html, :attachments
		
		def initialize(name, url)
			@name = name
			@url = url
			@rants = []
			@strips = []
			@shrunk = false
		end
		
		def format
			@text = ''
			@html = ''
			@text << " * " << @name << "\n"
			@html << "<h2><a href=\"#{@url}\">#{CGI.escapeHTML(@name)}</a></h2>\n"
			titles = []
			
			createAttachments
			@strips.each do |strip|
				# add a reference to the HTML
				titles << (title = strip[:title] || "")
				@html << "<figure style=\"margin:0 0 1em;\">" unless title.empty?
				@html << "<a href=\"#{strip[:url]}\"><img border=\"0\" src=\"cid:#{strip[:cid]}\" title=\"#{title}\" style=\"max-width: 100%\"/></a><br/>\n"
				@html << "<figcaption>#{title}"
				@html << "<br><small>(comic was resized, click on image to see original)<small>" if @shrunk
				@html << "</figcaption></figure>" unless title.empty?
				
				if @shrunk
					@text << "Attached comic was resized, click url to the original: #{strip[:url]}\n"
				end
			end
			
			@rants.each do |rant|
				ranttext = cleanHTML(rant[:data])
				@text << ranttext << "\n"
				next if titles.include? ranttext # don't repeat any of the titles that may have been captured automatically and already rendered in HTML
				# easy - just add the text with html formatting removed
				@html << "<div>#{rant[:data].gsub(/href=["']([^"']+)["']/) { |s| 'href="' + canonize($1, rant[:url]) + '"' }}</div>"
			end
			
			@html << "</p>\n"
		end
		
		def createAttachments
			@attachments = @strips.collect { |strip| newMIMEPart(strip[:cid], strip[:mime], strip[:filename], strip[:data]) }
		end
		
		def shrink
			if @shrunk then
				$logger.error("Feed #{name}(#{url}) already shrunk (now at #{payloadSize}b)! cannot reshrink!")
				raise RuntimeError.new("Feed #{name}(#{url}) already shrunk (now at #{payloadSize}b)! cannot reshrink!")
			end
			@shrunk = true
			@strips.each do |s|
				begin
					s[:data] = Comix::ImageUtils::scaleImage s[:data], s[:filename], 600.0, "#{name}:#{s[:filename]}(#{s[:mime]})"
				rescue Comix::ImageUtils::MissingImageImplementation => e
					$logger.error "Failed to shrink #{name}:#{s[:filename]}(#{s[:mime]}) - missing image implementation"
				end
			end
		end
		
		def cleanHTML html
			text = html.gsub(%r[(</p>|<br\s*/?>)],"\n")
			text = text.gsub(/<[^>]+>/,'')
			return CGI.unescapeHTML(text)
		end
		
		def add strip, filename
			@strips << { cid: genCID, mime: strip.mime, filename: filename, data: strip.data, url: strip.url, title: strip.title, id: strip.id }
		end
		
		def newMIMEPart cid, mimetype, filename, data
			#Content-ID: <1189814548.24932.0.camel@foundation>
			#Content-Disposition: inline; filename="Screenshot-Compose Message.png"
			#Content-Type: image/png; name="Screenshot-Compose Message.png"
			#Content-Transfer-Encoding: base64
			part = RMail::Message.new
			part.header.add('Content-ID', "<#{cid}>")
			part.header.add('Content-Disposition','inline',nil, 'filename' => filename)
			part.header.add('Content-Type', mimetype, nil, 'name' => filename)
			part.header.add('Content-Transfer-Encoding', 'base64')
			part.body = Base64.encode64(data)
			return part
		end
		
		def payloadSize
			self.format if @attachments.nil?
			@attachments.collect(&:to_s).collect(&:length).sum
		end
		
		def genCID
			"#{Time.now.to_i}.#{(rand * 1000000).floor}.#{Etc.getpwuid(Process::Sys.getuid).name}@geek.co.il"
		end
		
	end

	class ComixMessage
		def initialize(user, fake = false, maxSize = 0)
			GC.start # force cleanup of previous message data - our target runtime has a very aggressive OOM killer
			@user = user
			@fakemode = fake
			@maxSize = maxSize
			@title = "Daily comic strips"
			
			# additional parts that will be added (so I can add them after I finish building the text)
			@feeds = []
			
			# dictionary to prevent duplicate image filenames
			@imagefilenames = {}
			
			@text_preamble = "#{@title}\n#{'-' * @title.length}\n\n"
			@html_preamble = "<html><body>\n"
			@html_preamble << "<h1>#{@title}</h1>\n"
			@html_postamble = "</body></html>"
			
			# show message of the the day (if supplied)
			unless @user.message.nil? then
				@text_preamble << @user.message << "\n\n"
				@user.message.split("\n\n").each { |line| @html_preamble << "<p>#{line.strip}</p>\n" }
				@user.update_attribute('message',nil)
			end
			@currentfeed = nil
		end
		
		def payloadSize
			@feeds.collect(&:payloadSize).sum
		end
		
		def checkFeedChanges(item)
			# handle feed changes
			if @currentfeed.nil? or @currentfeed.name != item.feed.name then
				@feeds << (@currentfeed = ComixMessageFeed.new(item.feed.name, item.feed.homepage))
			end
		end	
		
		def addItem(item)
			checkFeedChanges(item)
			# append the item
			case item
			when ComixStrip 	then addStrip(item)
			when ComixRant		then addRant(item)
			when ComixText		then addRant(item)
			else
				raise FeedError, "Unknown item type #{item.class}: #{item}"
			end
		end
		
		def addRant(rant)
			$logger.debug("New rant for #{@currentfeed.name}")
			@currentfeed.rants << { data: rant.data, url: rant.url }
		end
		
		def addStrip(strip)
			raise FeedError, "Invalid mime type #{strip.mime} for #{strip}!" unless strip.mime[0,5] == 'image'
			# make sure I don't use duplicate file names
			strip.filename = 'image' if strip.filename.length < 1
			strip.filename += '.' + strip.mime.split('/').pop() if strip.filename.split('.').length < 2
			filename = String.new(strip.filename)
			dupid = 1
			while @imagefilenames.has_key?(filename) do
				$logger.debug("File name #{filename} is already in use, trying a new file name")
				filename = strip.filename.gsub(/\.([^\.]*)$/,".#{dupid}.\\1")
				raise IOError, "Can't modify file name for #{strip.feed.name}, #{strip.filename}" if filename == strip.filename
				dupid += 1
			end
			@imagefilenames[filename] = 1
			# create a new body part
			$logger.debug("New strip for #{@currentfeed.name}")
			@currentfeed.add strip, filename
			# enforce maxsize
			#return false if @maxSize > 0 && (@payloadSize + newPart.to_s.length > @maxSize)
		end
		
		def count
			@feeds.count
		end
		
		def collectText
			@text_preamble + @feeds.collect(&:text).join("\n")
		end
		
		def collectHTML
			@html_preamble + @feeds.collect(&:html).join("\n") + @html_postamble
		end
		
		def send
			begin
				mail = newMail(@user.name, @user.email, @title + " (" + @feeds.collect(&:name).join(", ") + ")")
				@feeds.each(&:format)
				
				$logger.info("Packing and sending email to #{@user}#{@fakemode ? '(faking it)' : ''}")
				# finally compose everything together
				contents = RMail::Message.new
				contents.header.add('Content-Type', 'multipart/alternative', nil, 'boundary' => genBoundary)
				textContent = RMail::Message.new
				textContent.header.add('Content-Type','text/plain', nil, 'charset' => 'utf-8')
				textContent.header.add('Content-Transfer-Encoding', 'quoted-printable')
				textContent.body = collectText.to_quoted_printable
				contents.add_part(textContent)
				htmlContent = RMail::Message.new
				htmlContent.header.add('Content-Type','text/html', nil, 'charset' => 'utf-8')
				htmlContent.header.add('Content-Transfer-Encoding', 'quoted-printable')
				htmlContent.body = collectHTML.to_quoted_printable
				contents.add_part(htmlContent)
				mail.add_part(contents)
				@feeds.collect(&:attachments).flatten.each { |p| mail.add_part(p) }
				#raise Net::SMTPFatalError, 'message file too big' if $debug_size_limit > 0 and $debug_size_limit < mail.to_s.length
				sendmail(mail) unless (@fakemode and @user.email != 'oded@geek.co.il')
			rescue Net::SMTPFatalError => e
				throw e unless e.message.include? 'message file too big'
				$logger.warn "Message file is too big (#{mail.to_s.length} bytes) (for #{@feeds.collect(&:name).join(', ')}), trying to shrink"
				@feeds.sort { |a,b| b.payloadSize <=> a.payloadSize}.first.shrink
				retry
			end
		end
		
		def sendmail(msg, from = nil, to = nil)
			from = msg.header.from.first.address if from.nil? and msg.is_a?(RMail::Message)
			to = msg.header.to.first.address if to.nil? and msg.is_a?(RMail::Message)
			msg = msg.to_s if msg.is_a?(RMail::Message)
			$logger.info("Sending SMTP message from #{from} to #{to}")
			smtp = Net::SMTP.new($SMTP_SERVER, $SMTP_PORT)
			smtp.disable_starttls
			smtp.start do |smtp|
				smtp.send_message msg, from, to
			end
		end
		
		def newMail(name, email, subject = "Daily Comic for #{Time.now.strftime('%a %b %d, %Y')}")
			m = RMail::Message.new
			m.header.add('Content-Type', 'multipart/related', nil, { 
																	'type' => 'multipart/alternative', 'boundary' => genBoundary })
			m.header.add('User-Agent','Daily Comix/3.0')
			m.header.add('From',RMail::Address.new("#{$SMTP_SENDER_NAME} <#{$SMTP_SENDER_EMAIL}>"))
			m.header.add('To', RMail::Address.new("#{name} <#{email}>"))
			m.header.add('Subject', subject)
			return m
		end
		
		def genBoundary
			"=-#{Base64.encode64(md5sum(Time.now.to_s + (rand * 100).floor.to_s)[0..8]).chomp}"
		end
		
	end
end
