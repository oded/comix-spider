class UsersCommand < CmdParse::Command
  def initialize
    super 'user'
    self.short_desc = "Manipulate user accounts"
    self.long_desc = "The 'user' command makes it possible to add, remove and modify user accounts.
    	It also allows to send daily messages to users."
	add_command(UserListCommand.new)
	add_command(UserAddCommand.new)
	add_command(UserRegsCommand.new)
	add_command(SendCommand.new)
	add_command(UserEnableCommand.new)
	add_command(UserDisableCommand.new)
	add_command(MessageCommand.new)
	#options = CmdParse::OptionParserWrapper.new do |opt|
	#  opt.on( '-l', '--list', 'List users' ) { self.listUsers }
	#end
  end
end
  
class UserListCommand < CmdParse::Command
  def initialize
	super 'list', takes_commands: false
    self.short_desc = "List users"
  end
  def execute()
  	puts "Listing users:"
  	ComixUser.all.each do |u|
  		puts u
  	end
  end
end

class UserAddCommand < CmdParse::Command
	def initialize
		@admin = false
		super 'add', takes_commands: false
		self.short_desc = 'Add a new user'
		self.options do |opt|
			opt.on("--admin", "Create an admin user") {|f| @admin = true }
		end
	end
	def execute(name, email)
		puts "Creating user account #{name}<#{email}>"
		ComixUser.create(name: name, email: email, admin: @admin)
	end
end

class UserRegsCommand < CmdParse::Command
	def initialize
		super 'subscriptions'
		self.short_desc = "Manage subscriptions"
		add_command(ListUserRegsCommand.new, default: true)
		add_command(AddUserRegCommand.new)
		add_command(DelUserRegCommand.new)
	end
end

class AddUserRegCommand < CmdParse::Command
	def initialize
		super 'add', takes_commands: false
		self.short_desc = "Add a user subscriptions"
	end

	def execute(name, feed)
		user = ComixUser.getUser(name)
		feed = ComixFeed.getFeed(feed)
		puts feed
		begin
			user.registrations << feed
		rescue => e
			raise e unless e.message =~ /Duplicate entry/ # add duplicate subscription is idempotent
		end
	end
end

class DelUserRegCommand < CmdParse::Command
	def initialize
		super 'del', takes_commands: false
		self.short_desc = "Remove a user subscriptions"
	end

	def execute(name, feed)
		user = ComixUser.getUser(name)
		feed = ComixFeed.getFeed(feed)
		user.registrations.delete(feed)
	end
end

class ListUserRegsCommand < CmdParse::Command
	def initialize
		super 'list', takes_commands: false
		self.short_desc = "Show user subscriptions"
	end

	def execute(name)
		user = ComixUser.getUser(name)
		user.registrations.each do |r|
		    puts r
		end
  	end
end

class SendCommand < CmdParse::Command
	def initialize
		super 'send', takes_commands: false
		@fakemode = false
		self.short_desc = "Manually check for new strips to be sent to a specific user or all users"
		self.options do |opt|
		  opt.on( '-f', '--fake', 'Fake sending - does not actually send anything, but does everything else including updating last-sent time.' ) { @fakemode = true }
		end
	end

	def execute(name = nil)
		if (name.nil?)
			ComixUser.all.each { |user| user.mailNewItems(@fakemode) }
		else
			user = ComixUser.getUser(name)
			$logger.debug("Starting single send to #{user}")
			user.mailNewItems(@fakemode, true)
		end
  	end
end

class MessageCommand < CmdParse::Command
	def initialize
		super 'message', takes_commands: false
		self.short_desc = "Set a daily message to be sent to a user or all users. To send to all users, do not specify a name."
	end

	def execute(name, *message)
		user = nil
		begin
			user = ComixUser.getUser(name)
		rescue CmdParse::InvalidArgumentError => e 
			# no user specified, we're sending to all and the first arg was the beginning of the message
			message.unshift name
		end
		message = message.join(" ")
		
		unless user.nil?
			user.update_attribute("message", message)
		else
			ComixUser.all.each do |user|
				user.update_attribute("message", message)
			end
		end
	end
end

class UserEnableCommand < CmdParse::Command
	def initialize(name = 'enable')
		super name, takes_commands: false
		self.short_desc = "Enable a user account"
		@enable = true
	end

	def execute(name)
		user = ComixUser.getUser(name)
		user.active = @enable
		user.save!
		$logger.debug "Activated user(#{@enable.inspect}): #{user.inspect}"
	end
end

class UserDisableCommand < UserEnableCommand
	def initialize
		super 'disable'
		self.short_desc = "Disable a user account"
		@enable = false
	end
end
