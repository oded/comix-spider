PODNAME := test-comix-spider
NAME := comix-spider

pod:
	podman pod create --name=$(PODNAME) --replace -p 127.0.0.1:8025:8025

smtpd: pod
	podman run --pod=$(PODNAME) --name=$(NAME)-smtp -d --replace \
		docker.io/mailhog/mailhog

database:
	podman run --pod=$(PODNAME) --name=$(NAME)-db -d --replace \
		-e MARIADB_RANDOM_ROOT_PASSWORD=1 \
		-e MARIADB_DATABASE=comix \
		-e MARIADB_USER=test \
		-e MARIADB_PASSWORD=secret \
		mariadb:latest --max-allowed-packet=20971520

test: pod smtpd database
	podman run --pod=$(PODNAME) --name=$(NAME) -ti -v ${PWD}:${PWD} -w ${PWD} \
		-e DSN='{"adapter":"mysql2","host":"$(NAME)-db","database":"comix","username":"test","password":"secret","flags":["TRANSACTIONS"],"reconnect": true}' \
		-e SMTP_PORT=1025 \
		ruby:3.0 bash -xec '\
			apt update && apt install -qy libjpeg-dev libpng-dev libwebp-dev;\
			./comix -d test-image-support;\
			./comix initdb;\
			./comix feed add XKCD https://xkcd.com/;\
			./comix feed parsers add XKCD strip '"'"'//*[@id="comic"]/img'"'"';\
			./comix feed parsers add XKCD rant '"'"'//*[@id="comic"]/img/@alt'"'"';\
			./comix feed parsers add XKCD rant '"'"'//*[@id="comic"]/img/@title'"'"';\
			./comix user add --admin "Oded Arbel" oded@geek.co.il;\
			./comix user subscriptions add oded@geek.co.il XKCD;\
			./comix -d run;\
		'
	podman run --pod test-comix-spider -ti --rm docker.io/mariadb:latest mariadb -h127.0.0.1 -utest -psecret comix -e '\
		select comixid,filename,url,mime,title,date,md5,LENGTH(data) from comix_strips;\
		select * from comix_rants;\
		select * from comix_users;\
		'
#	$(MAKE) clean

connect-db:
	podman run --pod $(PODNAME) -ti --rm docker.io/mariadb:latest mariadb -h127.0.0.1 -utest -psecret comix

clean:
	podman stop $(NAME)-db
	podman stop $(NAME)-smtp
	podman pod rm -f $(PODNAME)

enter:
	podman commit comix-spider comix-spider:current
	podman run --pod $(PODNAME) -ti --rm -v ${PWD}:${PWD} -w ${PWD} comix-spider:current /bin/bash || true
	podman image rm comix-spider:current

.PHONY: pod smtpd database test clean prepare
