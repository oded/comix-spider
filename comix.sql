-- MySQL dump 10.17  Distrib 10.3.22-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: comix
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comix_feeds`
--

DROP TABLE IF EXISTS `comix_feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comix_feeds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `homepage` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `updated` datetime DEFAULT NULL,
  `update_freq` int(11) DEFAULT 43200,
  `next_poll` datetime DEFAULT NULL,
  `delay` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT 1,
  `last_error` text DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=453 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comix_parsers`
--

DROP TABLE IF EXISTS `comix_parsers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comix_parsers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comixid` int(11) NOT NULL,
  `type` enum('page','strip','rant') CHARACTER SET latin1 NOT NULL,
  `parent_page` int(11) DEFAULT NULL,
  `regex` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1191 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comix_rants`
--

DROP TABLE IF EXISTS `comix_rants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comix_rants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comixid` int(11) NOT NULL DEFAULT 0,
  `url` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `data` mediumtext NOT NULL,
  `md5` varchar(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_rants` (`md5`)
) ENGINE=InnoDB AUTO_INCREMENT=113803 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comix_registrations`
--

DROP TABLE IF EXISTS `comix_registrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comix_registrations` (
  `userid` int(11) NOT NULL DEFAULT 0,
  `comixid` int(11) NOT NULL DEFAULT 0,
  `want_rants` tinyint(1) DEFAULT 1,
  UNIQUE KEY `userid` (`userid`,`comixid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comix_strips`
--

DROP TABLE IF EXISTS `comix_strips`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comix_strips` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `comixid` int(11) NOT NULL DEFAULT 0,
  `filename` varchar(256) DEFAULT NULL,
  `url` varchar(512) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `mime` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `title` varchar(255) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `md5` varchar(35) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` longblob NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dup_images` (`md5`),
  KEY `updates` (`date`),
  KEY `comixid` (`comixid`)
) ENGINE=InnoDB AUTO_INCREMENT=836451 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comix_users`
--

DROP TABLE IF EXISTS `comix_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comix_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `schedule` int(11) NOT NULL DEFAULT 86400,
  `last_sent` datetime DEFAULT NULL,
  `admin` tinyint(1) DEFAULT 0,
  `active` tinyint(1) DEFAULT 1,
  `password` varchar(20) DEFAULT NULL,
  `refcode` varchar(50) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `email_size` int(11) DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'comix'
--

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed
