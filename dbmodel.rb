#!/usr/bin/ruby

#$: << '/usr/share/rails/activerecord/lib'

require 'rubygems'
require 'active_record'
require 'json'

# kill table inheritence. I need to do this because I want to use the colum "type" 
# for my nefarious purposes, muahahahaha
class ActiveRecord::Base
	self.inheritance_column = 'type_id'
end

#ActiveRecord::Base.logger = Logger.new(STDERR)
#ActiveRecord::Base.colorize_logging = false
secrets_file = ENV['DSN_FILE'] || File.join(File.dirname(__FILE__),"/.dbsecrets.json")
dsn = JSON[ENV['DSN'] || File.read(secrets_file)]
ActiveRecord::Base.establish_connection	adapter:	dsn['adapter'],
					host:		dsn['host'],
					database:	dsn['database'],
					username:	dsn['username'],
					password:	dsn['password'],
					write_timeout: 300,
					encoding: 'utf8mb4',
					reconnect: true

# try to connect to database
tries = 3
begin
	ActiveRecord::Base.connection
rescue ActiveRecord::DatabaseConnectionError => e
	tries-=1
	$logger.debug "Failed to connect to database: #{e.message} (#{tries} remaining)"
	if tries > 0
		sleep 1
		retry
	end
end
unless ActiveRecord::Base.connected?
	$logger.error "Database failed to connect after 3 tries!"
	exit 1
end

# load the dbmodel parts
require 'feeds'
require 'users'

class InitDBCommand < CmdParse::Command
	def initialize
		super 'initdb', takes_commands: false
		self.short_desc = "initialize the database"
		self.long_desc = "This command will create the required tables in the specified database (that must already exist)."
	end
	
	def execute
		ActiveRecord::Schema[7.0].define do
			create_table :comix_feeds do |t|
				t.string :name, null: false, default: ''
				t.string :homepage, null: false, default: ''
				t.datetime :updated, null: true
				t.integer :update_freq, null: false, default: 43200
				t.datetime :next_poll, null: true
				t.integer :delay, null: true
				t.boolean :active, null: false, default: true
				t.text :last_error, null: true
			end
			
			add_index :comix_feeds, :name, unique: true
			
			create_table :comix_parsers do |t|
				t.integer :comixid, null: false
				t.column :type, "enum('page','strip','rant')", null: false
				t.integer :parent_page, null: true
				t.text :regex, null: true
			end

			create_table :comix_rants do |t|
				t.integer :comixid, null: false
				t.string :url, null: false, default: ''
				t.timestamp :date, null: false
				t.binary :data, null: false
				t.string :md5, null: true
			end
			
			add_index :comix_rants, :md5, unique: true

			create_table :comix_registrations do |t|
				t.integer :userid, null: false
				t.integer :comixid, null: false
				t.boolean :want_rants, null: false, default: 1
			end
			
			create_table :comix_strips do |t|
				t.integer :comixid, null: false
				t.string :filename, null: true
				t.string :url, null: false, default: ''
				t.string :mime, null: false, default: ''
				t.string :title, null: true
				t.timestamp :date, null: false
				t.string :md5, null: true
				t.column :data, 'longblob', null: false
			end
			
			add_index :comix_strips, :md5, unique: true
			add_index :comix_strips, :date
			add_index :comix_strips, :comixid

			create_table :comix_users do |t|
				t.string :name, null: false
				t.string :email, null: false
				t.integer :schedule, null: false, default: 86400
				t.datetime :last_sent
				t.boolean :admin
				t.boolean :active, default: true
				t.string :password
				t.string :refcode
				t.text :message
				t.integer :email_size, null: false, default: 0
			end
			
			add_index :comix_users, :name, unique: true
		end
	end
end
