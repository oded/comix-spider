class FeedsCommand < CmdParse::Command
	def initialize
		super 'feed'
		self.short_desc = "Setup comix feeds"
		self.long_desc = "Use 'feed' to setup comics feeds."
		add_command(FeedListCommand.new)
		add_command(FeedAddCommand.new)
		add_command(FeedEnableCommand.new)
		add_command(FeedDisableCommand.new)
		add_command(FeedSleepCommand.new)
		add_command(FeedPollCommand.new)
		add_command(FeedSetCommand.new)
		add_command(FeedParsersCommand.new)
		add_command(FeedRemoveCommand.new)	
	end
end

class FeedListCommand < CmdParse::Command
	def initialize
		super 'list', takes_commands: false
		self.short_desc = "List comic feeds"
	end
	def execute()
		puts "Listing feeds:"
		ComixFeed.all.each do |f| 
			puts f
		end
	end
end

class FeedAddCommand < CmdParse::Command
	def initialize() super 'add', takes_commands: false
	self.short_desc = 'Add another comic strip'
	self.long_desc = 'feed add <name> <url> [frequency] [strip-parser]'
	end
	def execute(comic, url, hours = 12, strip_exp = nil)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		raise CmdParse::InvalidArgumentError, "No URI was provided for the homepage!" if url.empty?
		raise CmdParse::InvalidArgumentError, "Invalid URI!" if url.empty? or !(url =~ %r{^https?://})
		freq = hours.to_i * 3600
		if freq == 0 # no frequence, but experssion was provided
			strip_exp = hours
			freq = 12 * 3600
		end
		raise CmdParse::InvalidArgumentError, "3rd parameter must be a number of seconds between checks" if hours == 0
		feed = ComixFeed.create(name: comic, homepage: url, update_freq: freq)
		if feed.errors.empty?
			puts "#{feed}"
			
			unless strip_exp.nil?
				FeedParsersAddCommand.new.createParsers(feed, 'strip', [ strip_exp ])
			end
		else
			puts "Failed to create feed: "
			p feed.errors
		end
	end
end

class FeedEnableCommand < CmdParse::Command
	attr :enable
	def initialize(name = 'enable')
		super name, takes_commands: false
		self.short_desc = 'Enable a disabled comic strip'
		@enable = true;
	end
	
	def execute (comic)
		ComixFeed.transaction do
			strip = ComixFeed.getFeed(comic)
			strip.active = @enable ? 1 : 0;
			strip.save!
		end
	end
end

class FeedDisableCommand < FeedEnableCommand
	def initialize() 
		super 'disable'
		self.short_desc = 'Disable an enabled comic strip'
		@enable = false
	end
end

class FeedSleepCommand < CmdParse::Command
	def initialize() 
		super 'sleep', takes_commands: false
		self.short_desc = 'Disable a comic strip for a specified number of days'
	end
	def execute(comic, days)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		raise CmdParse::InvalidArgumentError, "No sleep days specified!" if days.to_i == 0
		feed = ComixFeed.getFeed(comic)
		feed.update_attribute('next_poll', Time.now + (days.to_i * 86400))
	end
end

class FeedPollCommand < CmdParse::Command
	attr :force
	def initialize
		super 'poll', takes_commands: false
		self.short_desc = 'Poll a comic strip now'
		self.long_desc = "Checks for updates with the comic strip whose name was provided. ARGS must be the name of one comic strip."
		self.options do |opt|
			opt.on("-f", "--force", "Poll feed now even if backoff should be in effect") {|f| @force = true }
		end
	end

	def execute(comic)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		begin
			ComixFeed.getFeed(comic).poll(@force)
		rescue FeedError => e
			puts "Error polling feed: #{e}"
		end
	end
end

class FeedSetCommand < CmdParse::Command
	def initialize
		super 'set'
		self.short_desc = "Modify a comic feed"
		self.long_desc = "Use 'set' command to modify comic feed attributes. every set command takes two parameters, the name of the comic and the new value"
		add_command(FeedSetHomepageCommand.new)
		add_command(FeedSetNameCommand.new)
		add_command(FeedSetFrequencyCommand.new)
	end
end

class FeedSetHomepageCommand < CmdParse::Command
	def initialize
		super 'homepage', takes_commands: false
		self.short_desc = "Set the comic feed's homepage"
	end
	def execute(comic, url)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		feed = ComixFeed.getFeed(comic)
		raise CmdParse::InvalidArgumentError, "No URI was provided for the homepage!" if url.empty?
		raise CmdParse::InvalidArgumentError, "Invalid URI!" if url.empty? or !(url =~ %r{^https?://})
		feed.update_attribute('homepage', url)
	end
end

class FeedSetNameCommand < CmdParse::Command
	def initialize
		super 'name', takes_commands: false
		self.short_desc = "Set the comic feed's Title"
	end
	def execute(comic, title)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		feed = ComixFeed.getFeed(comic)
		raise CmdParse::InvalidArgumentError, "Invalid Title!" if title.empty? or ComixFeed.hasFeed(title)
		feed.update_attribute('name', title)
	end
end

class FeedSetFrequencyCommand < CmdParse::Command
	def initialize
		super 'frequency', takes_commands: false
		self.short_desc = "Set the comic feed's check frequency in days or seconds (default: 0.5 day)"
	end
	def execute(comic, frequency)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		feed = ComixFeed.getFeed(comic)
		frequency = frequency.to_i
		raise CmdParse::InvalidArgumentError, "Invalid frequency!" if frequency <= 0
		frequency *= 86400 if frequency < 1000
		feed.update_attribute('update_freq', frequency)
	end
end

class FeedRemoveCommand < CmdParse::Command
  def initialize
	  super 'del', takes_commands: false
    self.short_desc = "Delete a comic feed"
  end
  def execute(comic)
	raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
	feed = ComixFeed.getFeed(comic)
	$logger.debug("Deleting feed #{feed.name} and all dependancies")
	feed.parsers.each { |p| p.destroy }
	feed.strips.each { |s| s.destroy }
	feed.registrations.each { |r| feed.registrations.delete(r) }
	feed.destroy
  end
end

class FeedParsersCommand < CmdParse::Command
	def initialize
		super 'parsers'
		self.short_desc = "Manipulate feed parsers"
		self.long_desc = "Add and remove feed parsers. When run without an additional command, lists current parsers."
		add_command(FeedParsersListCommand.new, default: true)
		add_command(FeedParsersAddCommand.new)
		add_command(FeedParsersRemoveCommand.new)
		add_command(FeedParsersReplaceCommand.new)
	end
end 

class FeedParsersListCommand < CmdParse::Command
	def initialize 
		super 'list', takes_commands: false
		self.short_desc = "List parsers"
	end
	def execute(comic) 
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		i = 0
		feed = ComixFeed.getFeed(comic)
		ComixParser.where("comixid = ?", feed.id).each do |p|
		    puts "#{i += 1}. #{p[:type]}: #{p.regex}" 
		end
	end
end

class FeedParsersAddCommand < CmdParse::Command
	def initialize(name = 'add')
		super name, takes_commands: false
		self.short_desc = "Add a new parser"
		self.long_desc = "Add another parser for the specified strip. 
		The arguments are <feed name> <strip|rant> <regex> [<regex> [..]].
		When multiple regular expressions are provided, the first regular expression should generate a url to
		a page with the second regular expression will be run, and so forth. This creates compound parsers 
		that can iterate over a nested navigation tree and find the data."
	end
	def execute(comic, type, *expressions)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		feed = ComixFeed.getFeed(comic)
		raise CmdParse::InvalidArgumentError, "No parser type was provided!" if type.empty?
		raise CmdParse::InvalidArgumentError, "No regular expression was provided!" if expressions.empty?
		ComixFeed.transaction do
			createParsers(feed, type, expressions)
		end
	end
	
	def createParsers(feed, stype, args)
		parent = nil
		begin
			regex = args.shift
			if args.empty? then
				$logger.debug("Adding parser #{stype}:#{regex} to #{feed.name}, parent #{parent}")
				parser = feed.parsers.create(:type => stype, :regex => regex)
				raise FeedError,"Error creating parser: #{parser.errors.to_s}" unless parser.errors.empty?
				parser.update_attribute('parent_page', parent.id) unless parent.nil?
			else
				$logger.debug("Adding page parser #{regex} to #{feed.name}, parent #{parent}")
				parser = feed.parsers.create(:type => 'page', :regex => regex)
				raise FeedError,"Error creating parser: #{parent.errors.to_s}" unless parser.errors.empty?
				parser.update_attribute('parent_page', parent.id) unless parent.nil?
				parent = parser
				$logger.debug("Parent is #{parent}")
			end
		end until args.empty?
	end
end

class FeedParsersReplaceCommand < FeedParsersAddCommand
	def initialize
		super 'replace'
		self.short_desc = "Replace all existing parsers"
		self.long_desc = "Remove all the parsers for a strip and add a new parser with the specified type
		The arguments are <feed name> <strip|rant> <regex> [<regex> [..]].
		When multiple regular expressions are provided, the first regular expression should generate a url to
		a page with the second regular expression will be run, and so forth. This creates compound parsers 
		that can iterate over a nested navigation tree and find the data."
	end
	def execute(comic, type, *expressions)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		feed = ComixFeed.getFeed(comic)
		raise CmdParse::InvalidArgumentError, "No parser type was provided!" if type.empty?
		raise CmdParse::InvalidArgumentError, "No regular expression was provided!" if expressions.empty?
		
		ComixFeed.transaction do
			# remove old parsers
			ComixParser.where("comixid = ?", feed.id).each do |p|
				$logger.debug("removing #{p}")
				p.destroy
			end

			createParsers(feed, type, expressions)
		end
	end
end

class FeedParsersRemoveCommand < CmdParse::Command
	def initialize
		super 'del', takes_commands: false
		self.short_desc = 'Delete a feed parser by serial number'
	end
	def execute(comic, parser_id)
		raise CmdParse::InvalidArgumentError, "No comic name was provided!" if comic.empty?
		feed = ComixFeed.getFeed(comic)
		raise CmdParse::InvalidArgumentError, "No parser serial number was provided!" if parser_id.empty?
		num = parser_id.to_i
		i = 0
		ComixParser.where("comixid = ?", feed.id).each do |p|
			i += 1
			if i == num then
				$logger.debug("removing #{p}")
				p.destroy
			end
		end
	end
end

