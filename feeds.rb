
require 'net-utils'
require 'nokogiri'
require 'mimemagic'

$MAX_DELAY = 336

class FeedError < StandardError
end

class FeedFetchError < FeedError
end

class ParserFailedError < StandardError
end

class ComixFeed < ActiveRecord::Base
	has_many :parsers, -> { where "parent_page IS NULL" }, class_name: "ComixParser", foreign_key: "comixid"
	has_many :rants, :class_name => "ComixRant", :foreign_key => "comixid"
	has_many :strips, :class_name => "ComixStrip", :foreign_key => "comixid"
	has_and_belongs_to_many :registrations, :class_name => "ComixUser", 
					    :join_table => "comix_registrations", 
					    :foreign_key => "comixid", 
					    :association_foreign_key => "userid", 
					    :insert_sql => 'INSERT INTO comix_registrations (comixid, userid) VALUES (#{id},#{record.id})',
					    :uniq => true, :select => "comix_users.*"
	def to_s() "#{self[:id]}:#{name} <#{homepage}> (updated: #{updated}, delay: #{delay}, next poll: #{next_poll})" end
	def info() "#{self[:id]}:#{name}" end
	
	def self.nextToPoll
		self.order(:next_poll).each do |feed|
			yield feed
		end
	end
	
	# load one feed record
	def ComixFeed.getFeed(name)
		feeds = ComixFeed.where("name = ? OR homepage = ? OR id = ?", name, name, name)
		if feeds.length != 1
			raise CmdParse::InvalidArgumentError, feeds.empty? ? 
					"No feeds matching '#{name}' found!" : 
					"More then one feed matching #{name} was found, aborting!"
		end
		return feeds.first
	end
	
	# Check if a feed name exists
	def ComixFeed.hasFeed(name)
		feeds = ComixFeed.where("name = ?", name)
		return feeds.length > 0
	end
	
	# poll a feed and check for new strips
	# @param force whether to ignore backoff policy and poll it now
	def poll(force = false)
		return false unless active
		force = true if next_poll.nil? # always force if this is the first time I see this strip
		if !force and (next_poll > Time.now) then
			# only poll if needed or requested
			$logger.info("Backoff #{to_s}")
			return false
		end

		$logger.info("Polling #{self}#{(force)?' (no backoff)':''}")
		raise FeedError, "No homepage for #{name}" if homepage.nil? or !(homepage =~ /^https?:/)
		begin
			ComixFeed.transaction do
				if ComixParser.parse(homepage, parsers)
					$logger.debug "Updated #{self} at #{Time.now}"
					update_attribute('updated', Time.now) 
				end
				$logger.debug("Done polling")
				update_attribute('next_poll', Time.now + update_freq)
			end
			update_attribute('last_error', nil)
			update_attribute('delay', 0)
			return true
		rescue FeedError => e
			# make sure not to poll the site for at least another hour after a failure
			# If the feed was never polled successfully, we still set next_poll to now + 1 hour, we
			# don't lose information because for feeds that never were polled successfuly, "updated" 
			# will be null
			polldelay = if self.delay.nil? or self.delay < 1 then
				1
			elsif self.delay >= $MAX_DELAY then
				self.delay
			else
				self.delay * 2
			end
			update_attribute('delay', polldelay)
			update_attribute('next_poll', Time.now + (polldelay * 3600))
			num = if last_error and last_error.start_with? 'fetch:'
				last_error.split(':')[1].to_i
			else
				1
			end
			if e.is_a? FeedFetchError
				num += 1
				if num > 3
					$logger.error("3rd network error encountered while updating #{id}:#{name}: #{e}")
					num = 0
				end
				update_attribute('last_error', "fetch:#{num}:#{e.message}")
			else
				update_attribute('last_error', e.message)
				$logger.error("Error encountered while updating #{id}:#{name}#{(force)?' (no backoff)':''}: #{e}")
				$logger.error("Next poll is at #{next_poll}")
			end
		end
		return false
	end
	
	# get all the strips that are newer then a certain time (default to last day) 
	def getNewStrips(from = (Time.now - 86400))
		list = []
		$logger.debug("Strips: #{strips}")
		ComixStrip.all(conditions: [ 'comixid = ? AND date > ?', self[:id], from]).each do |s|
			list << s
		end
		return list
	end
	
	# get all the rants that are newer then a certain time (default to last day) 
	def getNewRants(from = (Time.now - 86400))
		list = []
		ComixRant.all(conditions: [ 'comixid = ? AND date > ?', self[:id], from]).each do |r|
			list << r
		end
		return list
	end
end

class ComixParser < ActiveRecord::Base
	belongs_to :feed, :class_name => "ComixFeed", :foreign_key => "comixid"
	belongs_to :parentParser, :class_name => "ComixParser", :foreign_key => "parent_page"
	has_many :childParsers, :class_name => "ComixParser", :foreign_key => "parent_page"
	self.inheritance_column = 'ruby_type'

	class ResultURL
		def initialize url
			@url = url
		end
		def to_s
			@url
		end
	end
	
	class ResultText
		def initialize text
			@text = text
		end
		def to_s
			@text
		end
	end

	def to_s() "#{feed.name}[#{self[:type]}]: #{parent_page.nil? ? '+ ' : "| +-\t"}#{regex}" end

	def validate_on_create
		case self[:type]
			when 'page' then return
			when 'strip' then return
			when 'rant' then return
		else
			errors.add("Invalid parser type #{self[:type]}")
		end
	end
    
	def ComixParser.parse(url, parsers)
		url = parseURI(url)
		raise FeedError, "No parsers for #{url}!" if parsers.empty?
		begin
			res = fetch(url)
		rescue => e
			raise FeedFetchError, "Failed to retrieve #{url}: [#{e.class}] #{e}"
		end

		status = false
		parseErrors = []
		parsers.each do |p|
			$logger.debug("Checking #{p} against #{url}")
			begin
			    status |= p.check(res.body,url,res.xml?)
			rescue ParserFailedError => e
			    parseErrors.push e
			end
		end
		raise FeedError,"All parsers failed: #{parseErrors.join(', ')}" if parseErrors.length == parsers.length
		return status
	end
	
	##
	# Extract image URL and possible title from IMG element
	def parseImage el
		title = el['title'] || el['alt']
		[
			title ? ResultText.new(title) : nil,
			ResultURL.new(unless el['src'].nil? or el['src'].empty? or el['src'].start_with? 'data:' # we hate data sources
				el['src']
			else # ugghhhhh...
				# find something better hiding somewhere - comic website developers like to get fancy with their Javascript
				el.attributes.select do |name,attr|
					attr.value[%r{^https?://}] or (attr.value.start_with?('/') and name.end_with?('src'))
				end.collect do |name,attr|
					attr.value
				end.first
			end)
		].compact
	end

	def scanPage(page, is_xml, want_image = false)
		case
		when ! regex[%r{^//}].nil? then # use an XPath scan
			begin
				Nokogiri.send(is_xml ? :XML : :HTML, page).xpath(regex).collect do |el|
					$logger.debug "Found XPath match #{el}"
					if want_image
						case
						when el.is_a?(Nokogiri::XML::Attr) then # src attribute (for image?)
							[ ResultURL.new(el.value) ]
						when el.is_a?(Nokogiri::XML::Text) then # user is pulling text? probably for title
							[ ResultText.new(el.content) ]
						when el.name == 'img' then
							parseImage(el)
						else # user wants a non-image element as the "image" - probably for title
							[ ResultText.new(el.search('.//text()').to_s.gsub(/\s+/," ").gsub(/(^\s+|\s+$)/, "")) ]
						end
					else
						[ ResultText.new(el.to_s) ] # mimic the structure of regex matches
					end
				end
			rescue Nokogiri::XML::XPath::SyntaxError => e
				raise ParserFailedError, e.to_s
			end
		else # use a regex scan
			page.scan(/#{regex}/).collect { |scans| scans.collect { |res| want_image ? ResultURL.new(res) : ResultText.new(res) } }
		end
	end

	# check if this page has matches for this parser and act acordingly
	# @param body http response body
	# @param url Addressable::URI that generated this response
	# @return whether check found anything
	def check(page, url, is_xml = false)
		raise ParserFailedError, "No match for [#{regex}]" if (m = scanPage(page, is_xml, self['type'] == 'strip')).empty?
		
		status = false
		lasttitle = nil
		strip = nil
		m.each do |captures|
			raise FeedError,"Error in regex - no captures" if captures.empty? or captures.first.nil?
			g = (captures.find { |c| c.is_a? ResultURL } || captures.first)
			$logger.debug("Parser matched: #{g.to_s.gsub(/\s+/,' ')[0..80]}")
			begin
				case self['type']
				when 'page' then
					newurl = canonize(g.to_s, url)
					status |= ComixParser.parse(newurl, childParsers)
				when 'strip' then
					if g.is_a? ResultText then # not what we expected, but we'll make do
						if strip.nil? # top caption
							lasttitle = g.to_s
						else
							strip.update_attribute('title', g.to_s)
						end
						next
					end
					newurl = canonize(g.to_s,url)
					begin
						res = fetch(newurl,10, { "referer" => url.display_uri.to_s } )
					rescue => e
						raise FeedError, "Failed to retrieve #{newurl}: [#{e.class}] #{e}"
					end
					
					$logger.debug("Storing strip with size #{res.body.length}")
					content_type = res.content_type
					if content_type.nil? or !content_type.start_with? 'image/' then # surprising mime type, try to detect instead
						mime = MimeMagic.by_magic(res.body)
						unless mime.nil?
							content_type = mime.type
							$logger.debug "Strip content type was #{content_type} instead of incorrect #{res.content_type}"
						end
					end
					raise FeedError, "[#{self.feed.info}] Not storing strip with incorrect content-type #{content_type}" unless content_type.start_with? 'image/'
					stripfile = newurl.basename.delete('/') # basename might contain '/' if the URL has no path
					stripfile = newurl.domain if stripfile.empty? # fallback in case life is weird
					$logger.debug("Target filename: #{stripfile} (#{content_type})")
					begin
						strip = feed.strips.create	filename: stripfile,
										url: newurl.to_s,
										mime: content_type,
										title: captures.size > 1 ? captures.find { |c| c.is_a? ResultText }.to_s : lasttitle,
										date: res.date,
										md5: res.md5,
										data: res.body
						lasttitle = nil
						status |= strip.errors.empty?
						if strip.errors.empty?
							$logger.info "New strip for #{self}"
						else
							$logger.error "Strip creation errors: #{strip.errors.inspect}"
							strip = nil
						end
					rescue Mysql2::Error::ConnectionError => e
						$logger.warn "Error in database access: #{e}, retrying"
						begin
							ActiveRecord::Base.connection.reconnect!
						rescue
							sleep 10
							retry     # will retry the reconnect
						else
							retry     # will retry the database_access_here call
						end
					end
				when 'rant' then
					text = g.to_s.gsub(%r{<script[^>]*>.*?</script>}m,'').gsub(%r{<(?!/?(?:p|a|img))[^>]*>}m,'')
					rant = feed.rants.create url: url.to_s, data: text, md5: md5sum(g.to_s), date: Time.now
					status |= rant.errors.empty?
					if rant.errors.empty?
						$logger.info "New rant for #{self}"
					else
						$logger.error "[#{self.feed.info}] Rant creation errors: #{rant.errors.inspect}"
					end
				else
					$logger.error("Invalid parser type #{self[:type]}: #{self}!")
				end
			rescue ActiveRecord::RecordNotUnique => e
				$logger.debug("[#{self.feed.info}] Not storing duplicate resource #{self}")
			end
		end
		
		return status
	end
end

class ComixStrip < ActiveRecord::Base
	belongs_to :feed, :class_name => "ComixFeed", :foreign_key => "comixid"
	def to_s() "Strip(#{feed.name}): #{filename} (#{mime}) [#{md5}]" end

	def validate_on_create
		if ! feed.strips.all(select: "id,comixid,filename,url,mime,date,md5", conditions: [ "md5 = ? AND filename = ?", md5, filename]).empty? then
			$logger.debug("Not storing duplicate strip #{filename}")
			errors.add("Can't store multiple copies of the same strip") 
		else
			$logger.info("Stored strip #{filename} [#{mime}] for #{feed.name}")
		end
	end
end

class ComixRant < ActiveRecord::Base
	belongs_to :feed, :class_name => "ComixFeed", :foreign_key => "comixid"
	def to_s() "Rant(#{feed}): #{data[0..40]}..." end

	def validate_on_create
		# don't remove HTML just yet, it might be useful
#		data.gsub!(/<[^>]+>/, '')
#		data.gsub!(/&\w+;/) { |m|
#			case m
#				when '&quot;' then '"'
#				when '&lt;' then '<'
#				when '&gt;' then '>'
#				when '&amp;' then '&'
#			end 
#		}
		# do compact spaces, it is HTML friendly after all
		data.gsub!(/\s+/, ' ')
		if ! feed.rants.all(conditions: [ "md5 = ?", md5]).empty? then
			$logger.debug("Not storing duplicate rant #{data[0..40]}")
			errors.add("Can't store multiple copies of the same strip") 
		else
			$logger.info("Stored rant for #{feed.name}")
		end
	end
end
